from os import path

bind = "192.168.1.108:7777"
#bind = "127.0.0.1:7777" 
accesslog = path.join(path.dirname(__file__), "logs/g-access.log")
errorlog = path.join(path.dirname(__file__), "logs/g-error.log")
loglevel = 'debug'
workers = 1