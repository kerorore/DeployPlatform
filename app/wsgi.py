import websocket, json, psutil, signal
from subprocess import Popen, PIPE, run
from _thread import start_new_thread
from socket import gethostname, gethostbyname
from app.py_modules import dispatcher
from flask import Flask, render_template, request, send_from_directory
from os import system, chdir, path, makedirs, walk

_path = path.dirname(path.realpath(__file__))
rel_path = path.join(_path, 'py_modules/')
svr_path = path.join(_path, 'server/')
log_path = path.join(_path, 'server_log/')
git_svr_path = '/Users/archon/GitServer/'
socket_url = "ws://"+gethostbyname(gethostname())+":7778/ws"

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = log_path

# normal funcitons
def get_req(field):
	return request.json[field]

def get_areq(field):
	return request.args.get(field)

def station(opt, app, *args):
	config = dispatcher.DisPatcher()

	if opt == 1:
		config.create_section(app)
	elif opt == 2:
		config.insert_app(app, args[0])
	elif opt == 3:
		return config.search_pid(app)
	elif opt == 4:
		return config.search_section(app)
	elif opt == 5:
		config.delete_proc(app)
	del config

def sending_message(app, sentence):
	ws = websocket.create_connection(socket_url)
	data = json.dumps({'id': app, 'msg': sentence, 'act': '2'})
	ws.send(data)
	ws.close()

# threads
def new_mission(app, port, cmd, *args):
	# execute in local!
	cl = False
	proc = Popen(['python3','watchdog.py', str(app), str(port), str(cmd).replace(" ", "%")], stdout=PIPE, stderr=PIPE, cwd=rel_path)
	while True:
		line = proc.stderr.readline().decode('utf-8')
		if line != '':
			cl = True
			sending_message(app, line)
		else:
			break

	proc.kill()

	if cl == True:
		sending_message(app, app+'__close__')

def proc_run(cmd, sec):
	return run(cmd, timeout=sec, stdout=PIPE).stdout

def shell_deploy(app, *args):
	chdir(rel_path)
	proc_run(['sh', 'deploy.sh', app, svr_path, git_svr_path], 60)

def shell_delete(app, *args):
	cmd = {
		'rm-app': ['rm', '-rf', path.join(svr_path, app)],
		'rm-git': ['rm', '-rf', path.join(git_svr_path, app+'.git')],
		'rm-log': ['rm', path.join(log_path, app+'.txt')],
	}

	if path.exists(path.join(log_path, app+'.txt')) is False: cmd.pop('rm-log')
	[proc_run(cmd, 30) for k, cmd in cmd.items()]

def check_state(port):
	return '0' if proc_run(['lsof', '-i', ':'+str(port)], 60) == b'' else '1'

# url routing
@app.route('/')
def home():
	return render_template('index.html')

@app.route('/test/state', methods=['POST'])
def test_state():
	d = [get_req(x) for x in ['app', 'port']]
	p, f =  check_state(d[1]), '1'

	for dirpath, dirname, files in walk(path.join(svr_path, d[0])):
		if files or dirname:
			f = '0'
	return p + f

@app.route('/test/check', methods=['GET'])
def test_check():
	app = get_areq('app')
	flag = station(4, app)
	if path.exists(path.join(svr_path, app)) and flag == '0':
		flag += '1'
	else:
		flag += '0'
	return flag

@app.route('/deploy/init', methods=['POST'])
def deploy_init():
	d = [get_req(x) for x in ['app', 'port', 'cmd']]
	station(1, d[0])
	start_new_thread(new_mission, (d[0], d[1], d[2]))
	return 'Create sucessful'

@app.route('/deploy/center', methods=['GET'])
def deploy_center():
	d = [get_areq(x) for x in ['app', 'mode']]

	if path.exists(path.join(svr_path, d[0])) is False:
		if d[1] == '1':
			start_new_thread(shell_deploy, (d[0],))
			return 'Deploy successful'
		elif d[1] == '2':
			return 'Delete failed'
	else:
		if d[1] == '1':
			return 'Deploy failed'
		elif d[1] == '2':
			shell_delete(d[0])
			return 'Delete successful'

@app.route('/deploy/ins', methods=['GET'])
def deploy_ins():
	d = [get_areq(x) for x in ['app', 'pid']]
	station(2, d[0], d[1])
	return 'Insert sucessful'

@app.route('/deploy/stop', methods=['POST'])
def deploy_stop():
	app = get_req('app')
	pid = station(3, app)

	try:
		pproc = psutil.Process(int(pid))
		station(5, app)
	except psutil.NoSuchProcess:
		return 'No such process'

	[ps.send_signal(signal.SIGKILL) for ps in pproc.children(recursive=True)]
	pproc.send_signal(signal.SIGKILL)
	return 'Delete successful'

@app.route('/deploy/download', methods=['GET'])
def deploy_download():
	return send_from_directory(app.config['UPLOAD_FOLDER'], get_areq('log')+'.txt', as_attachment=True)

if __name__ == '__main__':
	app.run(debug=True)
