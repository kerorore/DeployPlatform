from json import loads
from socket import gethostname, gethostbyname
from tornado import websocket, web, ioloop

p2p = {}
HOST = gethostbyname(gethostname())
PORT = 7778

class RootHandler(web.RequestHandler):
	def get(self):
		self.set_cookie('your', 'dad', domain=HOST)

class SocketServerHandler(websocket.WebSocketHandler):
	def check_origin(self, origin):
		return True

	def open(self):
		pass
		# print ('Connection initialized')

	def on_message(self, msg):
		self.m = loads(msg)
		try:
			if self.m['act'] == '1' and self.m['id'] not in p2p:
				p2p[self.m['id']] = self
				print ('Front-end client ensured: {}'.format(self.m['id']))

			elif self.m['act'] == '1' and self.m['id'] in p2p:
				self.write_message(self.m['id']+'__occupy__')
				print ('Attempting to connect: refused')

			elif self.m['act'] == '2' and self.m['id'] in p2p:
				js_client = p2p[self.m['id']]
				js_client.write_message(self.m['msg'])
				print (self.m['msg'])

			else:
				print ('Nothing happened')
				
		except Exception as e:
			print ('Error message: {}'.format(e))

	def on_close(self):
		try:
			if self.m['act'] == '1':
				del p2p[self.m['id']]
				print ('Front-end client disconnected: {}'.format(self.m['id']))
		except KeyError:
			pass

		# print ('Connection closed')

app = web.Application([
	(r'/', RootHandler),
	(r'/ws', SocketServerHandler)
])

if __name__ == '__main__':
	app.listen(PORT)
	print (HOST)
	try:
		ioloop.IOLoop.instance().start()
	except KeyboardInterrupt:
		print ('Shut down')

