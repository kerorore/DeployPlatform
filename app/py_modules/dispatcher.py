from os import path
import configparser

class DisPatcher:
	def __init__(self):
		self.c = configparser.ConfigParser()
		self.filepath = path.join(path.dirname(path.realpath(__file__)), 'user-data.config')
		self.c.read(self.filepath)

	def create_section(self, app):
		self.c.add_section(app)
		self.c.set(app, 'pid', '')
		self.saved_data('w')

	def insert_app(self, app, pid):
		self.c.set(app, 'pid', pid)
		self.saved_data('w')

	def search_pid(self, app):
		return self.c.get(app, 'pid')

	def search_section(self, app):
		return '1' if app in self.c else '0'

	def delete_proc(self, app):
		self.c.remove_section(app)
		self.saved_data('w')

	def saved_data(self, opt):
		with open(self.filepath, opt) as f:
			self.c.write(f)