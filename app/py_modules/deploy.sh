#!/bin/sh

file_dir="$2$1"
git_dir="$3$1.git"
# file_dir="/Users/archon/Desktop/Remoteku/app/server/$1"
# git_dir="/Users/archon/GitServer/$1.git"

mkdir $file_dir
mkdir $git_dir
cd $git_dir

git init --bare
git --bare update-server-info
git config core.bare false
git config core.worktree $file_dir
git config receive.denycurrentbranch ignore

cd ..
cp './post-receive' './'$1'.git/hooks/'

exit 0
