import requests, websocket, shlex, threading
from sys import argv
from json import dumps
from time import sleep
from socket import gethostname, gethostbyname
from subprocess import run, Popen, PIPE
from os import getpid, path, O_NONBLOCK
from fcntl import F_SETFL, F_GETFL, fcntl

class WatchDog:
	def __init__(self):
		_path = path.dirname(path.realpath(__file__))
		self.app, self.port, self.cmd = argv[1], argv[2], argv[3]
		self.socket_url = "ws://"+gethostbyname(gethostname())+":7778/ws"
		self.server_path = path.join(_path, '../server/'+self.app)
		self.log_path = path.join(_path, '../server_log/')
		self.cmd = shlex.split((self.cmd+'%'+self.port).replace('%', ' '))

	def escape(self, obj):
		line = obj.readline().rstrip()
		sign = ["", "[1m", "[22m", "[32m", "[33m", "[39m"]
		
		for s in sign:
			line = line.replace(s, "")
		return line

	def disable_block(self, proc):
		obj = [proc.stdout, proc.stderr]
		[fcntl(o, F_SETFL, fcntl(o, F_GETFL) | O_NONBLOCK) for o in obj]

	def send_msg(self, msg):
		ws = websocket.create_connection(self.socket_url)
		data = dumps({'id': self.app, 'msg': msg, 'act': '2'})
		ws.send(data)
		ws.close()

	def write_log(self, msg):
		with open(self.log_path+self.app+".txt", "a") as f:
			f.write(msg+'\n')

	def app_close(self, *args):
		sleep(args[0])
		self.send_msg(self.app+'__close__')

	def installation(self):
		compo_list = {'package.json':'npm', 'bower.json':'bower'}

		for init, struct in compo_list.items():
			if path.isfile(path.join(self.server_path, init)):
				self.send_msg('[ App Message: installing {} components ... ]\n'.format(struct))
				run([struct, 'install'], cwd=self.server_path)
				self.send_msg('[ App Message: {} installation finished ]\n'.format(struct))
			else:
				self.send_msg('[ App Message: no {} was detected ]\n'.format(init))

	def msg_out(self, proc):
		err_sent = False

		while True:
			msg_out, err_out = self.escape(proc.stdout), self.escape(proc.stderr)

			if msg_out != '':
				self.send_msg(msg_out)
				self.write_log(msg_out)
			elif err_out != '':
				self.send_msg(err_out)
				if not err_sent: 
					sent = True
					threading.Thread(target=self.app_close, args=(2,), name='auto_close').start()

	def main(self):
		self.installation()

		with Popen(self.cmd, stdout=PIPE, stderr=PIPE, cwd=self.server_path, universal_newlines=True) as proc:
			pid = str(getpid())
			if pid == '': self.send_msg(self.app+'__close__')
			requests.get("http://"+gethostbyname(gethostname())+":7777/deploy/ins", params={'app':self.app, 'pid': pid})
			self.disable_block(proc)
			self.msg_out(proc)

if __name__ == '__main__':
	dog = WatchDog()
	dog.main()