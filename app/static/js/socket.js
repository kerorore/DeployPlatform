var ws = undefined;

scrollBottom = function() {
  $('#show').scrollTop($('#show')[0].scrollHeight);
}

TornadoSocket = function(info) {
	ws = new WebSocket('ws://'+window.location.hostname+':7778/ws');

	ws.onopen = function(evt) {
		console.log('Server connected');
		ws.send(info);
		console.log('Client binding');
	};

	ws.onmessage = function(evt) {
		_info = JSON.parse(info);
		secret = _info['id']+'__close__';
		occupy = _info['id']+'__occupy__';

		if (evt.data == secret) {
			stop_app();
			$('#show').append('\n[ System Message: Application has been crashed, please check and restart it. ]\n');
		} else if (evt.data == occupy){
			$('#show').append('\n[ System Message: Application is using by another user, please try again later. ]\n\n[ System Message: Service closed at '+timestamp()+' ]\n');
			change_status(4);
		} else {
			$('#show').append(evt.data+'\n');
		}

		scrollBottom();
	};

	ws.onclose = function(evt) {
		console.log('Socket closed');
		ws = undefined;
	};

	ws.onerror = function(evt) {
		console.log(evt);
	};

}