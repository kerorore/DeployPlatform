var target = 0,
	link = 'archon@'+window.location.hostname+':~archon/GitServer/';

timestamp = function() {
	var t = new Date();
	return t
}

toggls_class = function(ele, c1, c2) {
	ele.removeClass(c1).addClass(c2);
}

get_field = function(type) {
	var app = $('#app').val().toLowerCase(),
	    port = $('#port').val(),
	    cmd = $('#cmd').val(),
		data_user = JSON.stringify ({
			"app":app,
			"port":port,
			"cmd":cmd
		}),
		data_client = JSON.stringify({
			"id": app,
			"act":'1',
			"msg":'initial'
		});

	if (type == 0) { return data_user; } else { return data_client; }
};

check_input = function() {
	var app = $('#app').val().toLowerCase(),
		info = get_field(1);

	if (app == '') {
		alert('請輸入 app 名稱！');
		return 'xx'
	}

	$('#app-info, #show').html('')

	$.ajax({
		url: '/test/check?app='+app,
	}).done(function(flag) {
		switch(flag) {
			case '00':
				change_status(1, app);
				break;
			case '01':
				change_status(2, app);
				break;
			case '10':
				change_status(3, app, info);
				break;
		}		
	});
}

deploy_app = function() {
	var app = $('#app').val();
	$.ajax({
		url: '/deploy/center?app='+app+'&mode=1',
	}).done(function(msg) {
		//console.log(msg);
		if (msg == 'Deploy successful') {
			alert('App 已建立，請將檔案推送到伺服器上再按下開始。');
			change_status(2, app);
		} else if (msg == 'Deploy failed') {
			alert('此 app 已存在！');
		}
	});
}

delete_app = function() {
	var app = $('#app').val();

	$.ajax({
		url: '/deploy/center?app='+app+'&mode=2',
	}).done(function(msg) {
		console.log(msg);
		if (msg == 'Delete successful') {
			alert('App 刪除成功！')
			change_status(1, app);
		} else if (msg == 'Delete failed') {
			alert('此 app 不存在！');
		}
	});	
}

stop_app = function() {
	var data = get_field(0),
		app = $('#app').val();
	ws.close();

	$.ajax({
		url: '/deploy/stop',
		data: data,
		type: 'POST',
		contentType: 'application/json'
	}).done(function(msg) {
		change_status(2, app);
		$('#show').append('\n[ System Message: Service closed at '+timestamp()+' ] \n');
	});
}

change_width = function(_link) {
	var board_width = _link.length*6.7;
	$('#clip').width(parseInt(board_width));
}

change_status = function(mod, app, info) {
	switch(mod) {
		case 1:
			$('#app-info').html('None');
			$('#start, #stop, #delete, label[name="delete"]').attr('disabled', true);
			$('#deploy, label[name="deploy"], input[type="text"], #check').attr('disabled', false);
			toggls_class($('label[name="deploy"]'), 'btn-default', 'btn-success');
			toggls_class($('#check'), 'btn-default', 'btn-info');
			toggls_class($('label[name="delete"]'), 'btn-danger', 'btn-default');
		 	$('#start, #stop, #clear, div[name="func"]').show();
		 	$('#clip, #Log').hide();
			break;
		case 2:
			$('#app-info').html('Idle');
			$('#clip-board').html(link+app+'.git');
			change_width(link+app+'.git');
			$("#logs").attr('href', '/deploy/download?log='+app);
			$('#stop, #deploy, label[name="deploy"]').attr('disabled', true);
			$('#start, #delete, label[name="delete"], input[type="text"], #check, #list').attr('disabled', false);
			toggls_class($('label[name="deploy"]'), 'btn-success', 'btn-default');
			toggls_class($('label[name="delete"]'), 'btn-default', 'btn-danger');
			toggls_class($('#list'), 'btn-default', 'btn-primary');
			toggls_class($('#check'), 'btn-default', 'btn-info');
		 	$('#start, #stop, #clear, #clip, div[name="func"], #Log').show();	
			break;
		case 3:
			TornadoSocket(info);
			$('#app-info').html('Running');
			$('#show').append('[ System Message: Service start at '+timestamp()+' ]\n\n');
			$('#stop').attr('disabled', false);
			$('#start, #deploy, input[type="text"], #check, #delete, label[name="deploy"], label[name="delete"], #list').attr('disabled', true);
			toggls_class($('label[name="deploy"]'), 'btn-success', 'btn-default');
			toggls_class($('label[name="delete"]'), 'btn-danger', 'btn-default');
			toggls_class($('#list'), 'btn-primary', 'btn-default');
			toggls_class($('#check'), 'btn-info', 'btn-default');
			$('#stop, #clear, div[name="func"], #Log').show();
			break;
		case 4:
			$('#start, #delete, label[name="delete"], #stop, #deploy, label[name="deploy"]').attr('disabled', true);
			$('input[type="text"], #check').attr('disabled', false);			
			toggls_class($('#check'), 'btn-default', 'btn-info');
			$('#stop, #clear, div[name="func"], #Log').hide();
			$('#state').show()
			$('#app-info').html('Running');
			break;
	}			
}

$('#app, #port, #cmd').keyup(function(){
	var	app = $('#app').val(),
		port = $('#port').val(),
		cmd = $('#cmd').val(),
		s_btn = $('#start');

	if (app != '' && port != '' && cmd != '') {
		s_btn.attr('disabled', false);
	} else if (app == '' || port == '' || cmd == '') {
		s_btn.attr('disabled', true);
	}
});

$('#app').bind({
	focus: function() {
		$('input[name="func"], div[name="func"], #clip').hide();
	},
	keypress: function(e) {
		if (e.keyCode == 13) check_input();
	}
});

$('#check').click(function() {
	check_input();
	$('#clip-board').html('');
});

$('#deploy').click(function() {
	if (confirm('確定要開始部署？')) {
		deploy_app();
	}
});

$('#delete').click(function() {
	if (confirm('確定要移除此 app?')) {
		delete_app();	
	}	
});

$('#clear').click(function() {
	$('#show').empty();
});

$('.dropdown-item').click(function() {
	$('#cmd').val(this.innerHTML);
});

$('#start').click(function (){
	var data = get_field(0),
		info = get_field(1),
		cmd = $('#cmd').val(),
		port = $('#port').val(),
		app = $('#app').val().toLowerCase(),
		regex = /^[0-9]+$/;

	if (!regex.test(port)) {
		alert('Port 只能為數字。');
		return
	} else if (cmd == '' || cmd.indexOf('rm') != -1) {
		alert('阿不就好棒棒 ^_^?');
		return
	} else if ($('#deploy, label[name="deploy"]').attr('disabled') == undefined) {
		alert('請先部署 app 至 server。');
		return
	}

	$.ajax({
		url: '/test/state',
		data: data,
		type: 'POST',
		contentType: 'application/json',
	}).done(function(flag) {	
		switch(flag) {
			case '00':
				$.ajax({
					url: '/test/check?app='+app,
				}).done(function(_flag) {
					if (_flag == '01') {
						$('#show').html('');
						$.ajax({
							url: '/deploy/init',
							data: data,
							type: 'POST',
							contentType: 'application/json',
						}).done(function() {
							console.log('App started');
							change_status(3, app, info);
							scrollBottom();
						});		
					} else if (_flag == '10') {
						alert('App 正在執行中，請稍後再試。');
					} else {
						alert('請先部署 app');
					}
				});
				break;
			case '01':
				alert('檔案尚未傳輸完畢，請稍候再試。');
				break;
			case '10':
				alert('Port 已被佔用，請重新輸入。');
				break;
			case '11':
				alert('Port 已被佔用且檔案尚未傳輸完畢，請稍後再試。');
				break;
		}
	});
});

$('#stop').click(function (){
	stop_app();
	scrollBottom();
});

